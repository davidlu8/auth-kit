<?php

declare(strict_types=1);

namespace AuthKit\JsonRpcSignature\Engine;

class ValueSignatureEngine implements SignatureEngineInterface
{
    /** @var array */
    private array $debugInfo = [];

    /**
     * @param array $params
     * @return string
     */
    public function generate(array $params): string
    {
        $originalString = '';
        foreach($params as $value) {
            $originalString .= $value;
        }
        $this->setDebugInfo('originalString', $originalString);
        return md5($originalString);
    }

    /**
     * @return array
     */
    public function getDebugInfo(): array
    {
        return $this->debugInfo;
    }

    /**
     * @param string $key
     * @param string $info
     * @return bool
     */
    protected function setDebugInfo(string $key, string $info): bool
    {
        $this->debugInfo[$key] = $info;
        return true;
    }
}