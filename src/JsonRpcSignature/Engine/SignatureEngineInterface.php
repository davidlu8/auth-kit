<?php

declare(strict_types=1);

namespace AuthKit\JsonRpcSignature\Engine;

interface SignatureEngineInterface
{
    /**
     * 生成签名
     * @param array $params
     * @return string
     */
    public function generate(array $params): string;

    /**
     * @return array
     */
    public function getDebugInfo(): array;
}