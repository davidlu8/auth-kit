<?php

declare(strict_types=1);

namespace AuthKit\Util;

class StringUtil
{
    /**
     * @return string
     */
    public static function getRequestId(): string
    {
        return md5(uniqid("", true));
    }

    /**
     * @param int $length
     * @return string
     */
    public static function getNonce(int $length = 8): string
    {
        $chars = 'abcdefghijklmnopqrstuvwxyz0123456789';
        $charLength = strlen($chars);
        $str = '';
        for ($i = 0; $i < $length; $i++) {
            $str .= substr($chars, mt_rand(0, $charLength - 1), 1);
        }
        return $str;
    }

}