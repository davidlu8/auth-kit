<?php

declare(strict_types=1);

namespace AuthKit\Tests\JsonRpcSignature\Engine;

use AuthKit\JsonRpcSignature\Engine\ValueSignatureEngine;
use AuthKit\Tests\TestCase;

class ValueSignatureEngineTest extends TestCase
{
    /** @var ValueSignatureEngine $valueSignatureEngine */
    protected ValueSignatureEngine $valueSignatureEngine;

    public function setUp()
    {
        $this->valueSignatureEngine = new ValueSignatureEngine();
    }

    public function dpGenerate()
    {
        return [
            [
                ['appId01', 1, 'sdfljksdf', '{"member_id": 1}', 'appSecret01'],
                '444bd80654197997906a209d8244dbd1',
            ],
            [
                ['appId02', 2, 'sdfljksdf', '{"member_id": 2}', 'appSecret02'],
                '0e143ae9a808c23c50e2b1fce92057f3',
            ],
            [
                ['appId03', 3, 'sdfljksdf', '{"member_id": 3}', 'appSecret03'],
                '78b50f33b9a66f946781b6ef93ba2e9b',
            ],
        ];
    }

    /**
     * @dataProvider dpGenerate
     * @param $params
     * @param $signature
     */
    public function testGenerate($params, $signature)
    {
        $this->assertEquals($signature, $this->valueSignatureEngine->generate($params));
    }

}