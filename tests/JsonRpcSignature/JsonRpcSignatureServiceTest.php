<?php

declare(strict_types=1);

namespace AuthKit\Tests\JsonRpcSignature;

use AuthKit\JsonRpcSignature\Engine\ValueSignatureEngine;
use AuthKit\JsonRpcSignature\JsonRpcSignatureService;
use AuthKit\Tests\TestCase;
use Contract\Exceptions\ValidationException;

class JsonRpcSignatureServiceTest extends TestCase
{
    /** @var JsonRpcSignatureService $jsonRpcSignatureService */
    protected JsonRpcSignatureService $jsonRpcSignatureService;

    public function setUp()
    {
        $this->jsonRpcSignatureService = new JsonRpcSignatureService(new ValueSignatureEngine());
    }

    /**
     * @return array[]
     */
    public function dpGenerate()
    {
        return [
            ['appId01', 1, 'sdfljksdf', '{"member_id": 1}', 'appSecret01', '444bd80654197997906a209d8244dbd1'],
        ];
    }

    /**
     * @dataProvider dpGenerate
     * @param $appId
     * @param $timestamp
     * @param $nonce
     * @param $body
     * @param $appSecret
     * @param $signature
     * @throws ValidationException
     */
    public function testGenerate($appId, $timestamp, $nonce, $body, $appSecret, $signature)
    {
        $this->assertEquals($signature, $this->jsonRpcSignatureService->generate($appId, $timestamp, $nonce, $body, $appSecret));
    }

    public function dpVerify()
    {
        return [
            ['appId01', 1, 'sdfljksdf', '{"member_id": 1}', 'appSecret01', '444bd80654197997906a209d8244dbd1', true],
            ['appId01', 1, 'sdfljksdf', '{"member_id": 1}', 'appSecret01', '444bd80654197997906a209d8244dbd12', false],
        ];
    }

    /**
     * @dataProvider dpVerify
     * @param $appId
     * @param $timestamp
     * @param $nonce
     * @param $body
     * @param $appSecret
     * @param $signature
     * @param $access
     */
    public function testVerify($appId, $timestamp, $nonce, $body, $appSecret, $signature, $access)
    {
        $this->assertEquals($access, $this->jsonRpcSignatureService->verify($appId, $timestamp, $nonce, $body, $appSecret, $signature));
    }
}